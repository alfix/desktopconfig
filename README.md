DesktopConfig WIP!
==================

Index

 1. [Introduction](#introduction)
 2. [Getting started](#getting-started)
 3. [Screenshots](#api)
 4. [Ideas and TODO](#todo-ideas)

Introduction
------------

*DesktopConfig* is an utility with a Text User Interface to install software
collections on [FreeBSD](https://www.freebsd.org), the script is focused on
a Desktop/Laptop Environment.

Getting started
---------------

	% fetch https://gitlab.com/alfix/desktopconfig/-/blob/master/desktopconfig
	% chmod a+x desktopconfig
	% sudo ./desktopconfig

For CURRENT

	# pkg update -f

Screenshots
-----------

**Start**

![Question in a dialog to start installation](screenshots/screenshot-start.png)

**GPU menu**

![Menu to choose gpu driver to install](screenshots/screenshot-gpu.png)

**Software selection menu**

![Menu to choose software to install](screenshots/screenshot-desktop.png)

**Progress installation**

![Bar shows installation progress](screenshots/screenshot-progress.png)

**End Message**

![Dialog with end installation message](screenshots/screenshot-end.png)


Ideas and TODO
--------------

Configure */usr/local/etc/X11/xorg.conf.d/20-drivers.conf*.

Wayland

```
0   Wayland         Environment                        off \
1   sway            Wayland                            off \
```

```
Wayland )
	toinstall="$toinstall wayland"
	;;
sway )
	tmpmsg="$tmpmsg hikari: \n \
		copy /usr/local/share/examples/hikari/hikari.conf to \n \
		~/.config/hikari/hikari.conf and edit it.\n"
	toinstall="$toinstall hikari"
	;;
```
